package model

import "context"

// 还需要定义接口
type Service interface {
	Hello(ctx context.Context, in *HelloRequest) (*HelloResponse, error)
}

type HelloRequest struct {
	Name string
}

type HelloResponse struct {
	Value string
}
