package impl

import (
	"fmt"

	"gitee.com/penghengben/rpc/protobuf/blog"
	"github.com/infraboard/mcube/ioc"
)

var _ blog.BlogRpcServer = &impl{}

func init() {
	ioc.RegistryController(&impl{})

	fmt.Println(ioc.ListControllerObjectNames())
	fmt.Println(ioc.GetController("GRPCBlog"))
}

type impl struct {
	// 继承 BlogRpcServer接口的实现模板
	blog.UnimplementedBlogRpcServer
	// 这也是一个模板，继承后就有基本的方法
	ioc.ObjectImpl
}

func (i *impl) Name() string {
	return "GRPCBlog"
}
