package impl

import (
	"bufio"
	"context"
	"fmt"
	"io"
	"os"

	"gitee.com/penghengben/rpc/protobuf/blog"
)

// CreateBlog 就是方法重写 ---> UnimplementedBlogRpcServer
func (i *impl) CreateBlog(ctx context.Context, in *blog.CreateBlogRequest) (
	*blog.Blog, error) {
	return &blog.Blog{Spec: &blog.CreateBlogRequest{
		Content: "grpc blog",
	}}, nil
}

// 定义一个流式请求
// 1. 不断的读取请求 保存到文件中
func (i *impl) UploadBlog(stream blog.BlogRpc_UploadBlogServer) error {
	for {
		data, err := stream.Recv()
		if err != nil {
			// 如果遇到io.EOF表示客户端流被关闭
			if err == io.EOF {
				return nil
			}
			return err
		}
		fmt.Println(data.Meta, string(data.Data))
	}
}

// 定义一个Server 流式请求
func (i *impl) DownloadBlog(
	in *blog.DownloadStreamRequest,
	stream blog.BlogRpc_DownloadBlogServer) error {
	// 服务端 不断发送请求给客户端
	fmt.Println(in.FileName)

	// 获取到stream client后不断的向服务端发送数据
	f, err := os.Open("README.md")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	r := bufio.NewReader(f)

	// conn
	for {
		line, _, err := r.ReadLine()
		if err != nil {
			return err
		}

		stream.Send(&blog.DownloadStreamResponse{Data: line})
		if err != nil {
			return err
		}
	}
}
