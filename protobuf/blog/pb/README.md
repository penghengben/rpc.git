# 业务定义

protoc-gen-go这个插件只生成 数据结构定义代码

```go
// 在项目目录进行代码生成
// /e/Code/go11-project/rpc/protobuf

protoc -I=. --go_out=. --go_opt=module="gitee.com/penghengben/rpc/protobuf" blog/pb/blog.proto
```

要生成接口定义的代码，需要使用独立的插件: protoc-gen-go-grpc 
启用插件 protoc-gen-go插件的参数: --go_out= --go_opt=
启用插件 protoc-gen-go-grpc插件参数: --go-grpc_out=. --go-grpc_opt="xx"

```go
// 安装插件
go install google.golang.org/grpc/cmd/protoc-gen-go-grpc@latest
// 在项目目录进行代码生成
// /e/Code/go11-project/rpc/protobuf

protoc -I=. --go_out=. --go-grpc_out=. --go_opt=module="gitee.com/penghengben/rpc/protobuf" blog/pb/blog.proto  --go-grpc_opt=module="gitee.com/penghengben/rpc/protobuf" blog/pb/blog.proto

// model.go <--- protoc-gen-go ---> blog.pb.go
// interface.go <--- protoc-gen-go-grpc ---> blog_grpc.pb.go
```