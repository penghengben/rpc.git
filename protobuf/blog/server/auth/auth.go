package auth

import (
	"context"
	"fmt"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
)

var (
	ClientIdKey     = "client-id"
	ClientSecretKey = "client-key"
)

// 认证中间件
func AuthInterceptor(ctx context.Context,
	req any,
	info *grpc.UnaryServerInfo,
	handler grpc.UnaryHandler) (
	resp any, err error) {
	// 1. 获取用户传递的 认证信息
	// http 1.1 header map[string][]string
	// http2/grpc meta == http2 header
	// 怎么获取Header，这个header信息是放到 请求的上下文中: ctx

	// 从ctx中获取 meta信息
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return nil, fmt.Errorf("meta data required")
	}

	ck := md.Get(ClientIdKey)
	if len(ck) == 0 {
		// grpc
		return nil, status.Errorf(codes.PermissionDenied, "ClientIdKey required")
	}
	sk := md.Get(ClientSecretKey)
	if len(sk) == 0 {
		return nil, fmt.Errorf("ClientSecretKey required")
	}

	if !(ck[0] == "admin" && sk[0] == "123456") {
		return nil, fmt.Errorf("auth failed")
	}

	// 路由给后面的请求处理
	return handler(ctx, req)
}
