package main

import (
	"fmt"
	"log"
	"net"
	"os"

	"gitee.com/penghengben/rpc/protobuf/blog"
	"github.com/infraboard/mcube/ioc"
	"google.golang.org/grpc"

	// 注册业务实现
	_ "gitee.com/penghengben/rpc/protobuf/blog/impl"
	"gitee.com/penghengben/rpc/protobuf/blog/server/auth"
)

func main() {
	// 1. 业务RPC对象 impl 已经实现了，托管到ioc去了

	// 2. 如何注册给grpc框架，客户端如何调用
	// 2.1 启动一个grpc的服务
	// 首先是通过grpc.NewServer()构造一个gRPC服务对象
	grpcServer := grpc.NewServer(grpc.UnaryInterceptor(auth.AuthInterceptor))

	// 2.2 把业务实现impl 注册到grpcServer上
	blog.RegisterBlogRpcServer(grpcServer, ioc.GetController("GRPCBlog").(blog.BlogRpcServer))

	// 2.3 启动grpc server
	lis, err := net.Listen("tcp", ":1234")
	if err != nil {
		log.Fatal(err)
	}
	err = grpcServer.Serve(lis)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
