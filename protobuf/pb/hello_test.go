package pb_test

import (
	"testing"

	"gitee.com/penghengben/rpc/protobuf/pb"
	"google.golang.org/protobuf/proto"
	"google.golang.org/protobuf/types/known/anypb"
)

type TestA struct {
	// TestA.A.(xxxx)
	A any
}

func TestHelloRequest(t *testing.T) {
	ins := &pb.HelloRequest{Value: "xxx"}
	t.Log(ins)
	data, err := proto.Marshal(ins)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(data)

	ins2 := &pb.HelloRequest{}
	err = proto.Unmarshal(data, ins2)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(ins2)
}

func TestEvent(t *testing.T) {
	ins := &pb.Event{Data: &pb.Event_Operate{}}
	t.Log(ins)

	ins.GetOperate()
}

// Example 4: Pack and unpack a message in Go
//
//	foo := &pb.Foo{...}
//	any, err := anypb.New(foo)
//	if err != nil {
//	  ...
//	}
//	...
//	foo := &pb.Foo{}
//	if err := any.UnmarshalTo(foo); err != nil {
//	  ...
//	}
func TestAnypb(t *testing.T) {
	msg := &pb.ErrorStatus{Message: "xxx"}
	// &pb.Blog{} &pb.BlogSet{} ...
	bany, err := anypb.New(&pb.AlertEvent{Name: "blog A"})
	if err != nil {
		t.Fatal(err)
	}
	msg.Detail = bany
	t.Log(msg)

	// any的序列化
	data, err := proto.Marshal(msg)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(data)

	// any的反序列化
	ins2 := &pb.ErrorStatus{}
	if err := proto.Unmarshal(data, ins2); err != nil {
		t.Fatal(err)
	}
	t.Log(ins2)

	// struct {a: }
	ae := &pb.AlertEvent{}
	// 此时 Detail 还处于未解析的状态，UnmarshalTo来解析成具体的类型
	err = ins2.Detail.UnmarshalTo(ae)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(ae)
}
