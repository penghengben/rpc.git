# Protobuf的定义

安装一个插件: vscode-proto3

hello.proto ---> 怎么生成一个Go语言的对象？依赖于一个protoc编译器

protoc是c++写的一个cli工具，用于辅助开发的，用于生成Go代码

protobuf的编译: 生成数据结构hello.proto ---> protoc ---> go语言的代码

```sh
$ protoc --version
libprotoc 25.1
```

## 编译

```sh
$ cd protobuf/ ## 以protobuf作为编译的工作目录
# option go_package="gitee.com/penghengben/rpc/protobuf/pb"
# pb/hello.proto ---> pb已存在，就不创建 直接把生成的文件放到pb下面
$ protoc -I=. --go_out=. --go_opt=module="gitee.com/penghengben/rpc/protobuf" pb/hello.proto
```

## any类型
```sh
$ cd protobuf/
$ protoc -I=. -I=/usr/local/include/ --go_out=. --go_opt=module="gitee.com/penghengben/rpc/protobuf" pb/hello.proto
```